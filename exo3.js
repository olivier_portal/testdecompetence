import {createAll} from './users.data';

const companies = createAll();

console.log('----EXO 3---', companies);

// Créer une fonction qui prend en paramètre la variable "companies"
// un age et qui enlève de toutes les
// companies les users qui ont moins que cet age

function suppressYounger(companies, ageLimit) {
  return companies
      .map((company) => {
        company.users = company.users
            .filter((user) => user.age > ageLimit);
        company.size = company.users.length;
        return company;
      });
}

console.log(suppressYounger(companies, 100));
