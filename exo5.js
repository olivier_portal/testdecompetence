import {createAll} from './users.data';

const companies = createAll();

console.log('----EXO 5---', companies);

// Créer une fonction qui prend en paramètre la variable "companies",
// un parametre "cars" (number) et qui renvoie un boolean s'il y a plus d'users
// qui ont une voiture que le paramètre cars dans TOUTES les companies

// exemple : si cars = 3 et qu'il y a au moins 3 users avec des voitures dans toutes les
// companies alors la fonction renvoie true. S'il y a une company où il y a seulement
// 2 personnes qui ont des voitures alors la fonction renvoie false

function exo5(companies, cars) {
  return companies
      .every((company) => {
        const carOwner = company.users
            .reduce((acc, user) => acc + user.car, 0);
        return carOwner > cars;
      });
}

console.log(exo5(companies, 2));
