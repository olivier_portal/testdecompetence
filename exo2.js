import {createAll} from './users.data';
const companies = createAll();

console.log('----EXO 2---', companies);

// Créer une fonction qui prend en paramètre la variable "companies" et qui renvoie
// le nombre d'users total de toutes les companies (exemple, s'il y a 3 company avec
// 10 user chacune, la fonction renvoie le nombre 30

function totalUsers(companies) {
  return companies.reduce((acc, company) => acc + company.size, 0);
}

console.log(totalUsers(companies));
