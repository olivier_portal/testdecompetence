import {createAll} from './users.data';

const companies = createAll();
console.log('----EXO 6---', companies);

// créer une fonction qui prend en paramètre la variable 'companies' et qui renvoie
// un objet dont les attributs sont la concaténation du nom, du prénom
// et de l'age des users de toutes les companies et dont la valeur
// est la valeur du boolean "car".
/*
const example = {
  johnDoe32: true,
  BernardMinet45: false,
  alinaChef23: true,
};

console.log(example);*/

function concatNamesAndAge(companies) {
  const obj = {};
  companies
      .reduce((acc, company) => acc.concat(company.users), [])
      .forEach((user) => obj[user.firstName + user.lastName + user.age] = user.car);
  return obj;
}

console.log(concatNamesAndAge(companies));
