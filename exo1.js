import {createAll} from './users.data';
const companies = createAll();

console.log('----EXO 1---', companies);

// Créer une fonction qui prend en paramètre la variable "companies",
// un argument "firstName" de type string et qui remplace tous les prénoms
// des users par cet argument et qui met une majuscule à tous les noms et
// prénoms des users et qui renvoie la variable companies avec ces modifications

function capi(name) {
  return name ? name.charAt(0).toUpperCase() + name.slice(1) : '';
}

function replaceAndCapiName(companies, newName) {
  return companies
      .map((company) => {
        company.users
            .map((user) => {
              user.firstName = capi(newName);
              user.lastName = capi(user.lastName);
              return user;
            });
        return company;
      });
}

console.log(replaceAndCapiName(companies, 'olivier'));
