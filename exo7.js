import {createAll} from './users.data';

const companies = createAll();

console.log('----EXO 7---', companies);

// créer une fonction qui prend en paramètre la variable "companies" et qui
// renvoi un array contenant des objets dont les attributs sont les valeurs des attributs
// des companies et les valeurs la concaténation de tous les prénoms des users

function exo7(companies) {
  return companies.map((company) => {
    const obj = {};
    Object.values(company)
        .forEach((value) => {
          if (typeof value !== 'object') {
            return obj[value] = company.users
                .reduce((acc, user) => acc + (user.firstName + user.lastName), '');
          }
        }); return obj;
  });
}

console.log(exo7(companies));
