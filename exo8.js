import {createAll} from './users.data';
const companies = createAll();

console.log('----EXO 8---', companies);

// Créer une fonction qui prend en paramètre la variable "companies" et qui calcule
// la moyenne d'âge de tous les users de toutes les companies (utilisation de reduce obligatoire)

function averageAge(companies) {
  const numberUsers = companies.reduce((acc, company) => acc + company.size, 0);
  const ageTotal = companies
      .reduce((acc, company) => acc.concat(company.users), [])
      .reduce((acc, user) => acc + user.age, 0);
  return (ageTotal / numberUsers).toFixed(2);
}

console.log(averageAge(companies));
