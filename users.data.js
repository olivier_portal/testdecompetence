const firstName = [
  'joe',
  'jane',
  'seb',
  'alina',
  'fabien',
  'merwan',
  'anna',
  'annah',
  'Fathma',
  'Mohamed',
  'Walid',
  'Josiane',
  undefined,
];
const lastName = [
  'Dupont',
  'Dupond',
  'Durand',
  'Kassir',
  'Dalachra',
  'Hussein',
  'Wartani',
  'Thoumsi',
  'Angello',
  undefined,
];

const companyName = ['Apple', 'ibm', 'facebook', 'Renault', 'Sony', 'Tempur'];

function createCompany() {
  const companies = [];
  for (let i = 0; i < createRandomNumber(3, 10); i++) {
    const companySize = createRandomNumber(1, 30);
    companies.push({
      name: getValue(companyName) + i,
      size: companySize,
      users: createUser(companySize),
    });
  }
  return companies;
}

function createRandomNumber(min, max) {
  return Math.floor(min + Math.random() * (max - min));
}

function getValue(array) {
  return array[createRandomNumber(0, array.length)];
}

function createUser(end) {
  const tab = [];

  for (let i = 0; i < end; i++) {
    tab.push({
      firstName: getValue(firstName),
      lastName: getValue(lastName),
      age: createRandomNumber(10, 120),
      car: !!createRandomNumber(0, 2),
    });
  }
  return tab;
}

export function createAll() {
  return createCompany();
}


