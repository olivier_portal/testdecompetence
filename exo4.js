import {createAll} from './users.data';
const companies = createAll();

console.log('----EXO 4---', companies);

// Créer une fonction qui prend en paramètre la variable "companies", une variable
// "car (boolean) et une variable age (number)
// qui récupères toutes les "users" de chaque
// company dans un seul tableau, puis qui mets en majuscule à tous leurs
// noms et prénom, puis qui ne garde que les personnes qui ont plus que le paramètre age
// et qui correspondent au paramètre car

function capi(name) {
  return name ? name.charAt(0).toUpperCase() + name.slice(1) : '';
}

function exo4(companies, carOwner, ageLimit) {
  return companies
      .reduce((acc, company) => acc.concat(company.users), [])
      .map((user) => {
        user.firstName = capi(user.firstName);
        user.lastName = capi(user.lastName);
        return user;
      })
      .filter((user) => user.age > ageLimit && user.car === carOwner);
}

console.log(exo4(companies, false, 50));
